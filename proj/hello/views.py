from django.shortcuts import render
from django.http import HttpResponse

from time import strftime


# Create your views here.

def index(request):
	return HttpResponse("<h1>Hello world</h1><p>The time is " + strftime('%c') + "</p>")


